<?php
use Cake\Routing\Router;

Router::plugin(
    'Chart',
    ['path' => '/chart'],
    function ($routes) {
        $routes->fallbacks('DashedRoute');
    }
);
