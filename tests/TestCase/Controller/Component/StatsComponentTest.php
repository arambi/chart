<?php
namespace Chart\Test\TestCase\Controller\Component;

use Cake\Controller\ComponentRegistry;
use Cake\TestSuite\TestCase;
use Chart\Controller\Component\StatsComponent;
use Chart\Controller\Component\ChartComponent;
use Cake\Network\Request;
use Cake\Network\Session;
use Cake\Controller\Controller;
use Cake\ORM\TableRegistry;
use Cake\Utility\Hash;

/**
 * Chart\Controller\Component\StatsComponent Test Case
 */
class StatsComponentTest extends TestCase
{

  public $fixtures = [
    'plugin.chart.orders',
  ];

  /**
   * setUp method
   *
   * @return void
   */
  public function setUp()
  {
      parent::setUp();
      $this->Controller = new Controller( new Request(['session' => new Session()]));
      $registry = new ComponentRegistry( $this->Controller);
      $this->Stats = new StatsComponent( $registry);
      $this->Chart = new ChartComponent( $registry);
      $this->Orders = TableRegistry::get( 'Shop.Orders');
  }

  /**
   * tearDown method
   *
   * @return void
   */
  public function tearDown()
  {
      unset( $this->Stats);
      unset( $this->Chart);

      parent::tearDown();
  }

  /**
   * Test initial setup
   *
   * @return void
   */
  public function testInitialization()
  {
    $title = 'El título';
    $ref = 'Datos';
    $line = $this->Chart->create( 'Datos', '#datos', array(
      'chart' => array(
        'type' => 'line',
        'marginRight' =>  130,
        'marginBottom' => 25,
        'zoomType' => 'x',
      ),
      'title' => array(
          'text' => 'El título'
      ),
      'axisTitle' => array(
          'y' => 'El título'
      ),
      'categories' => array(
          'x' => $this->Stats->getLegendValues()
      )
    ));

    $query = $this->Orders->find();

    $this->Stats
      ->conditions( 'Orders.created', $query)
      ->group( 'Orders.created', $query)
      ->select( 'Orders.created', $query);

    $entries = $this->Stats->build( $query);

    $line->addSerie( array(
        'name' => $title,
        'data' => array_map( 'intval', Hash::extract( $entries, '{n}.number'))
    ), $ref);
    
    $total = $this->Stats->total( $entries, 'number');
  }
}
